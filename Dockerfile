FROM ubuntu:18.04 as base

### Bellow is used because opencv installation asks for locations
ENV DEBIAN_FRONTEND noninteractive

RUN apt-get update \
  && apt-get install -y --no-install-recommends \
    apt-utils \
    sudo \
    wget vim \
    build-essential \
  && apt-get update \
  && apt-get install -y --no-install-recommends \
    gnupg2 \
    curl \
    vim \
    ca-certificates \
  && curl -fsSL https://developer.download.nvidia.com/compute/cuda/repos/ubuntu1804/x86_64/7fa2af80.pub | apt-key add - \
  && echo "deb https://developer.download.nvidia.com/compute/cuda/repos/ubuntu1804/x86_64 /" > /etc/apt/sources.list.d/cuda.list \
  && echo "deb https://developer.download.nvidia.com/compute/machine-learning/repos/ubuntu1804/x86_64 /" > /etc/apt/sources.list.d/nvidia-ml.list \
  && apt-get purge --autoremove -y curl \
  && rm -rf /var/lib/apt/lists/* 
  
ENV CUDA_VERSION 10.0.130

ENV CUDA_PKG_VERSION 10-0=$CUDA_VERSION-1

# For libraries in the cuda-compat-* package: https://docs.nvidia.com/cuda/eula/index.html#attachment-a
RUN apt-get update \
  && apt-get install -y --no-install-recommends \
    cuda-cudart-$CUDA_PKG_VERSION \
    cuda-compat-10-0 \
  && ln -s cuda-10.0 /usr/local/cuda \
  && rm -rf /var/lib/apt/lists/*

ENV CUDNN_VERSION 7.6.5.32

LABEL com.nvidia.cudnn.version="${CUDNN_VERSION}"

RUN apt-get update \
  && apt-get install -y --no-install-recommends \
    libcudnn7=$CUDNN_VERSION-1+cuda10.0 \
  && apt-mark hold libcudnn7 \
  && rm -rf /var/lib/apt/lists/* \
  && echo "/usr/local/nvidia/lib" >> /etc/ld.so.conf.d/nvidia.conf \
  && echo "/usr/local/nvidia/lib64" >> /etc/ld.so.conf.d/nvidia.conf \
  && apt-get clean


# nvidia-container-runtime
ENV NVIDIA_VISIBLE_DEVICES all
ENV NVIDIA_DRIVER_CAPABILITIES compute,utility
ENV NVIDIA_REQUIRE_CUDA "cuda>=10.0 brand=tesla,driver>=384,driver<385 brand=tesla,driver>=410,driver<411"

ENV PATH /usr/local/nvidia/bin:/usr/local/cuda/bin:${PATH}
ENV LD_LIBRARY_PATH /usr/local/nvidia/lib:/usr/local/nvidia/lib64:/usr/local/cuda/lib64

### create appliaction user
ARG DOCK_USER=cloud
RUN groupadd -g 999 $DOCK_USER \
  && useradd -r -m -u 999 -g $DOCK_USER $DOCK_USER \
  && usermod -a -G sudo $DOCK_USER
USER $DOCK_USER
WORKDIR /home/$DOCK_USER

USER root
### install anaconda
RUN wget --quiet https://repo.anaconda.com/miniconda/Miniconda3-py37_4.8.2-Linux-x86_64.sh -O miniconda3.sh \
  && bash miniconda3.sh -b -p /home/$DOCK_USER/miniconda3

ENV PATH /home/$DOCK_USER/miniconda3/bin:$PATH

## jemalloc update
RUN wget https://github.com/jemalloc/jemalloc/releases/download/5.2.1/jemalloc-5.2.1.tar.bz2 \
	&& tar xvf jemalloc-5.2.1.tar.bz2 \
	&& cd jemalloc-5.2.1 \
	&& ./configure \
	&& make \
	&& make install

# COPY ["./base.yml", "."]
# RUN conda env update -f ./base.yml

ARG BUILD_WITH_GPU=0
RUN bash -c 'if [[ ${BUILD_WITH_GPU} == 1 ]]; then \
  conda install pytorch=1.4.0 torchvision=0.5.0 cudatoolkit=10.0 -c pytorch; \
  conda install -c anaconda cudnn; \
  pip install tensorflow-gpu==1.15; \
  fi'

USER $DOCK_USER

FROM base AS production
# Copy code & models & app into image
# COPY [".", "/home/$DOCK_USER/code/."]
# COPY ["./src/models/.", "/home/$DOCK_USER/code/src/models/."]
# COPY ["./src/.", "/home/$DOCK_USER/code/src/."]

WORKDIR /home/$DOCK_USER/code
# RUN echo "ENTRYPOINT /bin/bash -c \"python3 analyser.py -s -n '${RQ_INSTANCE_NAME}' -i '${RQ_SERVICE_NAME}' -c '${RQ_CHANNEL}' -p '5672' -l '${RQ_LOGIN}' -r '${RQ_PWD}'\" "
# ENTRYPOINT /bin/bash -c "LD_PRELOAD=/usr/local/lib/libjemalloc.so python3 analyser.py -s -n '${RQ_INSTANCE_NAME}' -i '${RQ_SERVICE_NAME}' -c '${RQ_CHANNEL}' -p '5672' -l '${RQ_LOGIN}' -r '${RQ_PWD}'"
